"""
Created on 07 avr. 12:17 2020

@author: HaroldKS
"""

"""
Created on 11 fÃ©vr. 10:39 2020

@author: HaroldKS
"""
import math
import numpy as np


def r_squared(y_true, y_pred):  # Compute the R_squared on the observations and the true values

    mean = y_true.mean()
    result = 1 - sum([(i - j) ** 2 for i, j in zip(y_pred, y_true)]) / sum([(i - mean) ** 2 for i in y_true])
    return result


def rmse(y_true, y_pred):
    result = sum([(i - j) ** 2 for i, j in zip(y_pred, y_true)]) / len(y_true)
    return math.sqrt(result)


def cpc_loss(target, output):
    loss = 2 * np.minimum(output, target).sum() / (output.sum() + target.sum())
    return loss


def maple(target, output):
    up = np.abs(output - target)
    down = (np.abs(output) + np.abs(target)) / 2
    loss = np.sum(up / down)
    return loss
