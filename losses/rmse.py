"""
Created on 07 avr. 11:35 2020

@author: HaroldKS
"""

import torch
import torch.nn as nn


class RMSELoss(nn.Module):

    """
    Implementation of the classical root mean square error

    """

    def __init__(self):
        super(RMSELoss, self).__init__()
        self.mse_loss = nn.MSELoss()

    def forward(self, output, target):
        loss = torch.sqrt(self.mse_loss(output.float(), target.float()))
        return loss
