"""
Created on 07 avr. 11:29 2020

@author: HaroldKS
"""

import torch
import torch.nn as nn


class CPCLoss(nn.Module):

    """

    Implementation of the CPC loss as described in Caleb Robison article

    """

    def __init__(self):
        super(CPCLoss, self).__init__()

    def forward(self, output, target):
        loss = 1 - 2 * torch.min(output.float(), target.float()).sum() / (output.sum() + target.sum())
        return loss
