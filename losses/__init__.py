"""
Created on 07 avr. 11:29 2020

@author: HaroldKS
"""

from .rmse import RMSELoss
from .maple import MAPELoss
from .cpc import CPCLoss
