"""
Created on 07 avr. 11:35 2020

@author: HaroldKS
"""

import torch
import torch.nn as nn


class MAPELoss(nn.Module):

    def __init__(self):
        super(MAPELoss, self).__init__()

    def forward(self, output, target):
        up = torch.abs(output - target)
        down = (torch.abs(output) + torch.abs(target)) / 2
        loss = torch.sum(up / down)
        return loss
