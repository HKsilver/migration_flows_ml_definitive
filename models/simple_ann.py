"""
Created on 07 avr. 11:33 2020

@author: HaroldKS
"""

import torch.nn as nn


class SimpleNeuralNetwork(nn.Module):

    """
    A class to represent a shallow neural network with one hidden layer

    """

    def __init__(self, input_size, hidden_size, output_size):
        super(SimpleNeuralNetwork, self).__init__()
        self.layer_1 = nn.Linear(input_size, hidden_size)
        self.tan = nn.Tanh()
        self.layer_2 = nn.Linear(hidden_size, output_size)
        self.tan = nn.Tanh()

    def forward(self, x):
        out = self.layer_1(x)
        out = self.tan(out)
        out = self.layer_2(out)
        out = self.tan(out)
        return out
