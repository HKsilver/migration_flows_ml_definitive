"""
Created on 07 avr. 11:20 2020

@author: HaroldKS
"""

import torch.nn as nn


class DeepNeuralNetwork(nn.Module):
    """
    A class to represent a deep neural network with five hidden layers

    """

    def __init__(self, input_size, hidden_size, output_size, dropout):
        super(DeepNeuralNetwork, self).__init__()
        self.layer_1 = nn.Linear(input_size, hidden_size)
        self.relu = nn.ReLU()
        self.drop = nn.Dropout(p=dropout)
        self.layer_2 = nn.Linear(hidden_size, hidden_size // 2)
        self.relu = nn.ReLU()
        self.drop = nn.Dropout(p=dropout)
        self.layer_3 = nn.Linear(hidden_size // 2, hidden_size // 4)
        self.relu = nn.ReLU()
        self.drop = nn.Dropout(p=dropout)
        self.layer_4 = nn.Linear(hidden_size // 4, hidden_size // 8)
        self.relu = nn.ReLU()
        self.drop = nn.Dropout(p=dropout)
        self.layer_5 = nn.Linear(hidden_size // 8, output_size)
        self.tanh = nn.Tanh()

    def forward(self, x):
        out = self.layer_1(x)
        out = self.relu(out)
        out = self.drop(out)
        out = self.layer_2(out)
        out = self.relu(out)
        out = self.drop(out)
        out = self.layer_3(out)
        out = self.relu(out)
        out = self.drop(out)
        out = self.layer_4(out)
        out = self.relu(out)
        out = self.drop(out)
        out = self.layer_5(out)
        out = self.tanh(out)
        return out
