"""
Created on 07 avr. 11:41 2020

@author: HaroldKS
"""

import pandas as pd
import numpy as np
from patsy import dmatrices
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import torch
from skorch import NeuralNetRegressor
from models import DeepNeuralNetwork
from losses import RMSELoss
from metrics import *
import statsmodels.api as sm
from scatter_plots import scatter

METRICS = ['r_squared', 'rmse', 'cpc_loss']
torch.set_default_dtype(torch.float64)

if __name__ == '__main__':

    df = pd.read_csv("data/migration_other_to_oecd_v2.csv")

    # Sort values in the dataset if not
    df.sort_values(['iso_o', 'iso_d', 'year'])

    # Natural logarithm of available flows
    df['lnflow'] = np.log(df['flow'])

    # Lagged variables (shift to previous year)
    lag = ['conflict_o', 'conflict_d', 'disaster_o', 'disaster_d']
    for lagged in lag:
        df['l_' + lagged] = df[lagged].shift()

    # Variable to convert to natural logarithm
    to_log = ['pop_o', 'pop_d', 'rgdp_o', 'rgdp_d', 'distw']
    for var in to_log:
        df['ln' + var] = np.log(df[var])

    # Country specific ratio population rgdp
    df['rgdpc_o'] = df['rgdp_o'] / df['pop_o']
    df['lnrgdpc_o'] = np.log(df['rgdpc_o'])
    df['rgdpc2_o'] = df['rgdpc_o'] ** 2

    df['rgdpc_d'] = df['rgdp_d'] / df['pop_d']
    df['lnrgdpc_d'] = np.log(df['rgdpc_d'])
    df['rgdpc2_d'] = df['rgdpc_d'] ** 2

    # Linear interpolation and lagging for stock
    df['stock'].interpolate(inplace=True)
    df['lnstock_l'] = np.log(df['stock'] + 1)
    df['l_lnstock_l'] = df['lnstock_l'].shift()
    
    # Drop missing flows rows
    df.dropna(subset=['flow'], inplace=True)
    df.reset_index(inplace=True)
    df.drop(columns=['index'], inplace=True)
    
    # The sub dataset
    sub = "flow ~ lnpop_o + lnpop_d + lnrgdpc_o + lnrgdpc_d + lndistw + comrelig + l_lnstock_l + " \
          "comlang_off + colony  + fta_wto + l_conflict_o + l_conflict_d + l_disaster_o + " \
          "l_disaster_d + shendyadic + VISA + criterion + coverage"

    y, X = dmatrices(sub, data=df, return_type='dataframe')
    X.drop(columns=['Intercept'], inplace=True)
    X['year'] = df.year

    # Preprocessing for machine learning model
    scale_X = StandardScaler()
    X.iloc[:, 3:10] = scale_X.fit_transform(X.iloc[:, 3:10])

    scale_y = MinMaxScaler(feature_range=(-1, 1))
    y = scale_y.fit_transform(y)
    X['flow'] = y

    # Split in train and test set
    X_train = X[X.year <= 2000]
    y_train = X_train['flow']
    y_train = y_train.values.reshape(-1, 1)
    X_train.drop(columns=['flow', 'year'], inplace=True)
    X_train = X_train.values

    X_test = X[X.year > 2000]
    y_test = X_test['flow']
    y_test = y_test.values.reshape(-1, 1)
    X_test.drop(columns=['flow', 'year'], inplace=True)
    X_test = X_test.values

    # The model
    net = NeuralNetRegressor(
        module=DeepNeuralNetwork,
        module__input_size=X_train.shape[-1],
        module__output_size=y.shape[-1],
        module__hidden_size=200,
        module__dropout=0.0,
        iterator_train__shuffle=True,
        max_epochs=200,
        criterion=RMSELoss,
        batch_size=4,
        warm_start=True,
        optimizer=torch.optim.RMSprop,
        lr=0.00003,
        verbose=1)

    # Fitting the model
    net.fit(X_train, y_train)
    
    
    # Model results
    y_pred = net.predict(X_test)
    y_train_predicted = net.predict(X_train)
    real_values_y_test_predicted = scale_y.inverse_transform(y_pred)
    real_values_y_train_predicted = scale_y.inverse_transform(y_train_predicted)
    real_values_y_train = scale_y.inverse_transform(y_train)
    real_values_y_test = scale_y.inverse_transform(y_test)

    print("[INFO] Evaluation on the testing set : ")
    print(f"The R squared gives : {r_squared(real_values_y_test, real_values_y_test_predicted)}")
    print(f"The RMSE gives : {rmse(real_values_y_test, real_values_y_test_predicted)}")
    print(f"The CPC gives : {cpc_loss(real_values_y_test, real_values_y_test_predicted)}")
    print()
    print("[INFO] Evaluation on the train set : ")
    print(f"The R squared gives : {r_squared((real_values_y_train), (real_values_y_train_predicted))}")
    print(f"The RMSE gives : {rmse(real_values_y_train, real_values_y_train_predicted)}")
    print(f"The CPC gives : {cpc_loss(real_values_y_train, real_values_y_train_predicted)}")
    print()

    # Full sample prediction
    X.drop(columns=['flow', 'year'], inplace=True)
    y_full_pred = net.predict(X.values)
    y = scale_y.inverse_transform(y)
    y_full_pred = scale_y.inverse_transform(y_full_pred)
    X['flow_hat'] = y_full_pred
    X['flow'] = y
    X['year'] = df.year
    print("[INFO] Evaluation on the full set : ")
    print(f"The R squared gives : {r_squared(y, y_full_pred)}")
    print(f"The RMSE gives : {rmse(y, y_full_pred)}")
    print(f"The CPC gives : {cpc_loss(y, y_full_pred)}")
    print()

    # Poisson Model
    yp, Xp = dmatrices(sub, data=df, return_type='dataframe')
    yp_train, Xp_train = dmatrices(sub, data=df[df.year <= 2000], return_type='dataframe')
    yp_test, Xp_test = dmatrices(sub, data=df[df.year > 2000], return_type='dataframe')

    # Fitting the poisson model
    poisson_res = sm.GLM(yp_train, Xp_train, family=sm.families.Poisson()).fit()
    print(poisson_res.summary())
    Xp_test = sm.add_constant(Xp_test)
    yp_pred = poisson_res.predict(Xp_test)
    yp_hat = poisson_res.predict(Xp)
    X['pflow_hat'] = yp_hat

    # In the df we have all estimates for both poisson and ml model. We save them
    # X.to_csv("data/migV2_with_estimates", index=False)

    # Here the saved dataframe (can be used directly)
    # X = pd.read_csv("data/migV2_with_estimations_ml_poisson.csv")

    #Plots

    samples = ['in', 'out',  'full']

    for sample in samples:
        scatter(X, sample=sample, ml_model=True)
        scatter(X, sample=sample, ml_model=False)
