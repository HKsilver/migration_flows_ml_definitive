"""
Created on 07 avr. 12:21 2020

@author: HaroldKS
"""

import numpy as np
import matplotlib.pyplot as plt
from metrics import r_squared

ALPHA = 0.3
SIZE = 130
MARKERS = [",", "o", "v", "^", "<", ">", "."]
COLORS = ['g', 'b', 'c', 'm', 'y', 'k', 'r']
DIRECTORY = "figures"

def scatter(df, sample='out', ml_model=True, log=False, fmt='pdf', dpi=100):

    sub = df
    flow_hat = 'flow_hat' if ml_model else 'pflow_hat'
    name = 'ANN' if ml_model else 'Poisson'
    log_text = 'Log' if log else 'Absolute'

    if sample == 'out':
        sub = df[df.year > 2000]
    elif sample == 'in':
        sub = df[df.year <= 2000]
    elif sample == 'full':
        pass
    if log:
        sub = sub[sub.flow > 0.0]

    r2 = r_squared(sub['flow'].values, sub[flow_hat].values)
    plt.figure(figsize=(15, 10))
    plt.subplot(111, facecolor='gainsboro')

    if sample in ['in', 'full']:
        for period in range(1960, 1981, 20):

            i = (period - 1960) // 20
            xi = sub[(sub.year >= period) & (sub.year < period + 20)][flow_hat].values
            yi = sub[(sub.year >= period) & (sub.year < period + 20)]['flow'].values
            mi = MARKERS[i]
            ci = COLORS[i]
            if log:
                jxi = xi + 0.7 * np.random.rand(len(xi)) - 0.05
                jyi = yi + 0.7 * np.random.rand(len(yi)) - 0.05
                plt.scatter(jxi, jyi, marker=mi, color=ci, s=SIZE, alpha=ALPHA, label=f"{period}-{period + 20}")
            else:
                plt.scatter(xi, yi, marker=mi, color=ci, s=SIZE, alpha=ALPHA, label=f"{period}-{period + 20}")

    if sample in ['out', 'full']:
        for period in range(2000, 2010, 10):

            i = (period - 1960) // 10 - 2
            xi = sub[(sub.year >= period) & (sub.year < period + 10)][flow_hat].values
            yi = sub[(sub.year >= period) & (sub.year < period + 10)]['flow'].values
            mi = MARKERS[i]
            ci = COLORS[i]
            if log:
                jxi = xi + 0.7 * np.random.rand(len(xi)) - 0.05
                jyi = yi + 0.7 * np.random.rand(len(xi)) - 0.05
                plt.scatter(jxi, jyi, marker=mi, color=ci, s=SIZE, alpha=ALPHA, label=f"{period}-{period + 10}")

            else:
                plt.scatter(xi, yi, marker=mi, color=ci, s=SIZE, alpha=ALPHA, label=f"{period}-{period + 10}")

    x = np.linspace(sub.flow.min(), sub.flow.max())
    plt.plot(x, x, c='r')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Predicted flows', fontsize=20)
    plt.ylabel('Observed flows', fontsize=20)
    plt.title(f" {name}  {sample}  sample | {log_text} | R-squared = {r2:.3f} | Linear stock", fontsize=24)
    plt.legend()
    plt.grid(True)
    plt.savefig(f'{DIRECTORY}/{name}_{sample}_{log_text}_sample_scatter.{fmt}', format=fmt, dpi=dpi)
    plt.show()
